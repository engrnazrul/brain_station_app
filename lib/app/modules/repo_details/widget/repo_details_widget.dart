import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../common/ui.dart';
import '../../../providers/laravel_provider.dart';
import '../../global_widgets/circular_loading_widget.dart';
import '../controller/repo_details_controller.dart';
import 'repo_details_loader_widget.dart';

class RepoDetailsWidget extends GetView<RepoController> {
  RepoDetailsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (Get.find<LaravelApiClient>().isLoading(task: 'getRepoDetails')) {
        return RepoDetailsLoaderWidget();
      } else if (controller.isLoading.value) {
        return CircularLoadingWidget(height: 300);
      } else {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: Ui.getBoxDecoration(),
          width: Get.width * 1,
          height: Get.height * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)),
                child: CachedNetworkImage(
                  height: 100,
                  width: 100,
                  fit: BoxFit.cover,
                  imageUrl: controller.repoDetails.value.avatarUrl ?? "",
                  placeholder: (context, url) => Image.asset(
                    'assets/img/loading.gif',
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: 100,
                  ),
                  errorWidget: (context, url, error) => Icon(Icons.error_outline),
                ),
              ),
              SizedBox(height: 10),
              Text("Owner Name: ${controller.repoDetails.value.name ?? ""}".tr, style: Get.textTheme.titleLarge,),
              Divider(),
              Text("Repository Description: ${controller.description.value}".tr, style: Get.textTheme.bodyMedium,),
              SizedBox(height: 10),
              Text("Last Update Date: ${controller.updatedDate.value}".tr, style: Get.textTheme.bodyMedium,)
            ],
          ),
        );
      }
    });
  }
}