import 'package:get/get.dart';
import '../models/repo_list_model.dart';
import '../providers/laravel_provider.dart';

class RepoListRepository {
  late LaravelApiClient _laravelApiClient;

  RepoListRepository() {
    this._laravelApiClient = Get.find<LaravelApiClient>();
  }

  Future<RepoListModel> getRepoList(int page) {
    return _laravelApiClient.getRepoList(page);
  }
}
