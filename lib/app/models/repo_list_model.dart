class RepoListModel {
  int? totalCount;
  bool? incompleteResults;
  List<Items>? items;

  RepoListModel({this.totalCount, this.incompleteResults, this.items});

  RepoListModel.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_count'] = this.totalCount;
    data['incomplete_results'] = this.incompleteResults;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  int? id;
  String? name;
  bool? private;
  Owner? owner;
  String? description;
  String? createdAt;
  String? updatedAt;
  String? pushedAt;
  int? stargazersCount;

  Items(
      {this.id,
        this.name,
        this.private,
        this.owner,
        this.description,
        this.createdAt,
        this.updatedAt,
        this.pushedAt,
        this.stargazersCount,
        });

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    private = json['private'];
    owner = json['owner'] != null ? new Owner.fromJson(json['owner']) : null;
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    pushedAt = json['pushed_at'];
    stargazersCount = json['stargazers_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['private'] = this.private;
    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }
    data['description'] = this.description;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['pushed_at'] = this.pushedAt;
    data['stargazers_count'] = this.stargazersCount;
    return data;
  }
}

class Owner {
  String? login;
  int? id;

  Owner(
      {this.login,
        this.id,
      });

  Owner.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    data['id'] = this.id;
    return data;
  }
}