/*
 * File name: home_view.dart
 * Last modified: 2023.12.21
 * Author: Brain Station 23 - https://brainstation-23.com/
 * Copyright (c) 2023
 */

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/home_controller.dart';
import '../widgets/repo_list_widget.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 1.5,
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        leadingWidth: 150,
        leading: Obx(() => Align(
              alignment: Alignment.center,
              child: Text(
                "Total: ${controller.repoList.value.totalCount ?? ""}".tr,
                style: Get.textTheme.titleSmall,
              ),
            )),
        title: Text(
          "GitHub Repository".tr,
          style: Get.textTheme.titleLarge,
        ),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () async {
                controller.sortByStarCount();
                controller.isReverse.value = !controller.isReverse.value;
                controller.isShort.value = true;
              },
              icon: Obx(() => RotationTransition(
                turns: AlwaysStoppedAnimation(controller.isReverse.value ? 0.5 : 0),
                child: Icon(Icons.move_up_outlined, color: controller.isShort.value == false ? Colors.grey : Colors.blue, size: 20,),
              )))
        ],
      ),
      body: RepoListWidget(),
    );
  }
}
