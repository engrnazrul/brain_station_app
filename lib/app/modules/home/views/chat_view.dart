import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatView extends StatefulWidget {
  const ChatView({super.key});

  @override
  State<ChatView> createState() => _ChatViewState();
}

class _ChatViewState extends State<ChatView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Conversation".tr , style: Get.textTheme.titleLarge),
        centerTitle: true,
      ),
      body: Center(child: Text("No data found".tr , style: Get.textTheme.titleLarge),),
    );
  }
}
