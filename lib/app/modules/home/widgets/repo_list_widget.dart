import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../providers/laravel_provider.dart';
import '../controllers/home_controller.dart';
import 'repo_list_item_widget.dart';
import 'repo_list_loading_widget.dart';

class RepoListWidget extends GetView<HomeController> {
  RepoListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (Get.find<LaravelApiClient>().isLoading(task: 'getRepoList') &&
          controller.page == 1) {
        return RepoListLoaderWidget();
      } else {
        return SmartRefresher(
            controller: controller.refreshController,
            enablePullUp: true,
            enablePullDown: true,
            onLoading: () => controller.getRepoList(),
            onRefresh: () => controller.getRepoList(refresh: true),
            footer: CustomFooter(
              builder: (BuildContext? context, LoadStatus? mode) {
                Widget? body;
                if (mode == LoadStatus.idle) {
                  body = const Text("");
                  controller.refreshController.loadComplete();
                } else if (mode == LoadStatus.loading) {
                  body = CircularProgressIndicator(
                    color: Colors.blue,
                  );
                  controller.refreshController.loadComplete();
                } else if (mode == LoadStatus.failed) {
                  body = const Text("Load Failed!");
                  controller.refreshController.loadComplete();
                } else if (mode == LoadStatus.canLoading) {
                  body = const Text("release to load more");
                } else {}
                return mode == LoadStatus.idle
                    ? Container()
                    : SizedBox(
                        height: 20.0,
                        child: Center(child: body),
                      );
              },
            ),
            child: ListView.builder(
              padding: EdgeInsets.only(bottom: 10, top: 10),
              itemCount: controller.loadedData.length,
              itemBuilder: ((_, index) {
                if (index == controller.loadedData.length) {
                  return Obx(() {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new Center(
                        child: new Opacity(
                          opacity: 1,
                          child: new CircularProgressIndicator(),
                        ),
                      ),
                    );
                  });
                } else {
                  var _repoItems = controller.loadedData.elementAt(index);
                  return RepoListItemWidget(items: _repoItems);
                }
              }),
            ));
      }
    });
  }
}
