import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../common/ui.dart';
import '../../../models/repo_list_model.dart';
import '../../../routes/app_routes.dart';
import 'package:intl/intl.dart' show DateFormat;

class RepoListItemWidget extends StatelessWidget {
  final Items items;

  const RepoListItemWidget({super.key, required this.items});

  @override
  Widget build(BuildContext context) {
    String originalDateString = items.updatedAt!;
    DateTime originalDate = DateTime.parse(originalDateString);
    String updatedDate = DateFormat('MM-dd-yy HH:mm').format(originalDate);

    return GestureDetector(
        onTap: () {
          Get.toNamed(Routes.REPO_DETAILS, arguments: {
            "ownerLogin": items.owner?.login,
            "description": items.description,
            "updatedDate": updatedDate,
          }, );
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            decoration: Ui.getBoxDecoration(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Repository: ${items.name}".toUpperCase().tr,
                  style: Get.textTheme.bodyMedium,
                  maxLines: 3,
                ),
                Text(
                  "Repository ID: ${items.id}".tr,
                  style: Get.textTheme.labelSmall,
                  maxLines: 3,
                ),
                Text(
                  "Last Updated Date: $updatedDate".tr,
                  style: Get.textTheme.labelSmall,
                  maxLines: 3,
                ),
                SizedBox(height: 10,),
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(
                        "Star Gazers Count".tr,
                        maxLines: 1,
                        overflow: TextOverflow.fade,
                        softWrap: false,
                        style: Get.textTheme.labelMedium,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Align(
                        alignment: AlignmentDirectional.centerEnd,
                        child: Text(
                          items.stargazersCount.toString().tr,
                          style: Get.textTheme.titleLarge
                              ?.merge(TextStyle(color: Get.theme.focusColor)),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            )));
  }
}
