/*
 * File name: repo_view.dart
 * Last modified: 2023.12.20
 * Author: Brain Station 23 - https://brainstation-23.com/
 * Copyright (c) 2023
 */

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controller/repo_details_controller.dart';
import '../widget/repo_details_widget.dart';

class RepoDetailsView extends GetView<RepoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Theme.of(context).focusColor),
        elevation: 1.5,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        centerTitle: true,
        title: Text(
          "Repository Details".tr,
          style: Get.textTheme.titleLarge,
        ),
      ),
      body: RepoDetailsWidget()
    );
  }
}
