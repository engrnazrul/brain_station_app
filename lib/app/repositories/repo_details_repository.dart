import 'package:get/get.dart';
import '../models/repo_owner_model.dart';
import '../providers/laravel_provider.dart';

class RepoDetailsRepository {
  late LaravelApiClient _laravelApiClient;

  RepoDetailsRepository() {
    this._laravelApiClient = Get.find<LaravelApiClient>();
  }

  Future<RepoOwnerModel> getRepoDetails(String owner) {
    print(owner);
    return _laravelApiClient.getRepoDetails(owner);
  }
}
