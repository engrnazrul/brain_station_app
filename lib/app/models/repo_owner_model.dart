class RepoOwnerModel {
  String? login;
  int? id;
  String? avatarUrl;
  String? name;
  String? location;
  String? updatedAt;

  RepoOwnerModel(
      {this.login,
        this.id,
        this.avatarUrl,
        this.name,
        this.location,
        this.updatedAt});

  RepoOwnerModel.fromJson(Map<String, dynamic> json) {
    login = json['login'];
    id = json['id'];
    avatarUrl = json['avatar_url'];
    name = json['name'];
    location = json['location'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this.login;
    data['id'] = this.id;
    data['avatar_url'] = this.avatarUrl;
    data['name'] = this.name;
    data['location'] = this.location;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}