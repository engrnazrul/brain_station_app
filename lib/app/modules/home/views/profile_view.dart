import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({super.key});

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Profile".tr , style: Get.textTheme.titleLarge),
        centerTitle: true,
      ),
      body: Center(child: Text("No data found".tr , style: Get.textTheme.titleLarge),),
    );
  }
}
