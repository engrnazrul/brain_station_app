import 'package:flutter/material.dart';
import 'package:get/get.dart';
class CreateView extends StatefulWidget {
  const CreateView({super.key});

  @override
  State<CreateView> createState() => _CreateViewState();
}

class _CreateViewState extends State<CreateView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Repo".tr , style: Get.textTheme.titleLarge),
        centerTitle: true,
      ),
      body: Center(child: Text("No data found".tr , style: Get.textTheme.titleLarge),),
    );
  }
}
