import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../common/ui.dart';

class RepoDetailsLoaderWidget extends StatelessWidget {
  const RepoDetailsLoaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
            baseColor: Colors.grey.withOpacity(0.1),
            highlightColor: Colors.grey[200]!.withOpacity(0.1),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: Ui.getBoxDecoration(),
              width: Get.width * 1,
              height: Get.height * 0.5,
            ),
          );
  }
}
