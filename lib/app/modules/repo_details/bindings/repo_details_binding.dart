/*
 * File name: repo_details_binding.dart
 * Last modified: 2023.12.20
 * Author: Brain Station 23 - https://brainstation-23.com/
 * Copyright (c) 2023
 */

import 'package:get/get.dart';
import '../controller/repo_details_controller.dart';

class RepoDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(RepoController(), permanent: false);
  }
}
