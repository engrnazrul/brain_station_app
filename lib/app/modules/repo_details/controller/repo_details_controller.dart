/*
 * File name: home_controller.dart
 * Last modified: 2023.12.19
 * Author: Brain Station 23 - https://brainstation-23.com/
 * Copyright (c) 2023
 */

import 'package:get/get.dart';
import '../../../../common/ui.dart';
import '../../../models/repo_list_model.dart';
import '../../../models/repo_owner_model.dart';
import '../../../repositories/repo_details_repository.dart';

class RepoController extends GetxController {
  late RepoDetailsRepository _repoDetailsRepository;
  final isLoading = true.obs;

  final repoDetails = RepoOwnerModel().obs;
  final items = Items().obs;
  final ownerLogin = ''.obs;
  final updatedDate = ''.obs;
  final description = ''.obs;


  RepoController() {
    _repoDetailsRepository = new RepoDetailsRepository();
  }

  @override
  Future<void> onInit() async {
    ownerLogin.value = Get.arguments ["ownerLogin"];
    updatedDate.value = Get.arguments ["updatedDate"];
    description.value = Get.arguments ["description"];
    refreshRepoDetails();
    super.onInit();
  }


  Future refreshRepoDetails({bool showMessage = false}) async {
    await getRepoDetails();
    if (showMessage) {
      Get.showSnackbar(Ui.SuccessSnackBar(message: "Details page refreshed successfully".tr));
    }
  }


  Future getRepoDetails() async {
    try {
      repoDetails.value = await _repoDetailsRepository.getRepoDetails(ownerLogin.value);
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }finally{
      isLoading.value = false;
    }
  }
}
