/*
 * File name: home_controller.dart
 * Last modified: 2023.12.19
 * Author: Brain Station 23 - https://brainstation-23.com/
 * Copyright (c) 2023
 */

import 'dart:async';
import 'dart:io';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../../../common/ui.dart';
import '../../../models/repo_list_model.dart';
import '../../../models/repository_list_model.dart';
import '../../../repositories/repo_list_repository.dart';
import '../../../repositories/slider_repository.dart';

class HomeController extends GetxController {
  final GetStorage _getStorage = GetStorage();

  late SliderRepository _sliderRepo;
  late RepoListRepository _repoListRepo;
  final isReverse = false.obs;
  final isShort = false.obs;
  final isHigh = false.obs;
  final page = 1.obs;
  RxDouble scrollOffset = 0.0.obs;

  final sliderList = <Slide>[].obs;
  final repoList = RepoListModel().obs;
  RxList<Items> itemList = <Items>[].obs;
  RxList<Items> loadedData = <Items>[].obs;
  final RefreshController refreshController = RefreshController(initialRefresh: false);

  HomeController() {
    _repoListRepo = new RepoListRepository();
    _sliderRepo = new SliderRepository();
  }

  @override
  Future<void> onInit() async {
    initGetStorage();
    await refreshHome();
    startPeriodicFetching();
    super.onInit();
  }
  Future refreshHome({bool showMessage = false}) async {
    await getRepoList();
    if (showMessage) {
      Get.showSnackbar(Ui.SuccessSnackBar(message: "Repo list refreshed successfully".tr));
    }
  }

  void initGetStorage() {
    _getStorage.writeIfNull('repoData', <Items>[]); // Initialize repoData if not exists
  }

  void saveDataToGetStorage(List<Items> data) {
    _getStorage.write('repoData', data); // Save repoData
  }

  List<Items> getDataFromGetStorage() {
    final storedData = _getStorage.read<List<Items>>('repoData') ?? [];
    return storedData; // assign repoData when device offline
  }

  Future getRepoList({bool refresh = false}) async {
    try {
      if(refresh){
        page.value = 1;
        loadedData.clear();
      }
      List<Items> newData;
      try{
        repoList.value = await _repoListRepo.getRepoList(page.value);
        newData = repoList.value.items!;
        saveDataToGetStorage(newData);
        loadedData.addAll(newData);
        itemList.assignAll([...loadedData]);
      }catch(error){
        newData = getDataFromGetStorage();
        loadedData.addAll(newData);
        itemList.assignAll([...loadedData]);
      }
      page.value++;
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }finally{
      if (refresh) {
        refreshController.refreshCompleted();
      } else {
        refreshController.loadComplete();
      }
    }
  }

  // Sorting function to sort the list by rating count
  void sortByStarCount() {
    if(isReverse.value == false){
      loadedData.sort((a, b) => b.stargazersCount!.compareTo(a.stargazersCount!));
    }else{
      loadedData.sort((a, b) => a.stargazersCount!.compareTo(b.stargazersCount!));
    }
    loadedData.refresh();
  }

  // Fetch data from git repository in every 30 minutes
  void startPeriodicFetching() {
    const Duration duration = Duration(minutes: 30);

    Timer.periodic(duration, (Timer timer) async {
      await getRepoList(refresh: true);
    });
  }

  //3rd party slider for home page view
  Future getSlider() async {
    try {
      sliderList.assignAll(await _sliderRepo.getHomeSlider());
    } catch (e) {
      Get.showSnackbar(Ui.ErrorSnackBar(message: e.toString()));
    }
  }
}
